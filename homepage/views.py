from django.shortcuts import render

# Create your views here.
def about(request):
    return render(request, 'about.html')
def educations(request):
    return render(request, 'educations.html')
def organizations(request):
    return render(request, 'organizations.html')
def experiences(request):
    return render(request, 'experiences.html')
