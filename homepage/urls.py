from django.urls import path

from .views import *
app_name = 'homepage'

urlpatterns = [

    path('',about, name='about'),
	path('organizations',organizations, name='organizations'),
	path('educations',educations, name='educations'),
	path('experiences',experiences, name='experiences'),
    # dilanjutkan ...
]